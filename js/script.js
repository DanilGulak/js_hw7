const cityArr = ['Kiev', 'Dubai', 'Oslo', 'New York', 'Paris', 'London'];

// ------- OR ------------//
// let ulElement = document.createElement('ul');
//
// document.body.appendChild(ulElement);

const createList = (arr) => {
    let ulElement = document.createElement('ul');

    document.body.appendChild(ulElement);

    arr.map((element) => {
        let liElement = document.createElement('li');
        ulElement.appendChild(liElement);
        liElement.innerText = `${element}`;
    });
};

createList(cityArr);

